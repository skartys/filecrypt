//
//  ViewController.m
//  filecrypt
//
//  Created by Karthik Kumar S on 4/15/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import "ViewController.h"
#import "KeychainItemWrapper.h"
#import "KKCrypto.h"
#import "KKCryptoInputStream.h"
#import "KKCryptoKeyDerivationSpec.h"
#import "KKCryptoCryptorSpec.h"
#import "KKCryptor.h"
#import "PAPIEncryptedFile.h"

NSString * const USER_PASSWORD          = @"REMOVED";
NSString * const PARSE_REST_API_KEY     = @"REMOVED";
NSString * const PARSE_APPLICATION_ID   = @"REMOVED";


@interface ViewController () <NSURLConnectionDataDelegate,NSURLConnectionDelegate>

@property (nonatomic,strong) KKCryptor       * decryptor;
@property (nonatomic,strong) NSURLConnection * connection;
@property (nonatomic,strong) NSMutableString * status;

@end

@implementation ViewController

- (void) updateStatus:(NSString *)status {
    
    if (!_status) {
        self.status = [[NSMutableString alloc] init];
    }
    [self.status appendString:status];
    [self.status appendString:@"\n"];
    statusView.text = self.status;
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    //[self.decryptor initiateCryptor];
}


- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    //NSData * decrypted = [self.decryptor updateData:data isFinal:NO];
    NSString * str = [[NSString alloc] initWithBytes:data.bytes
                                              length:data.length
                                            encoding:NSUTF8StringEncoding];
    NSLog(@"%@",str);
//    NSString * str = [data base64EncodedStringWithOptions:-1];
//        NSLog(@"%@",str);

}


- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
   //NSData * decrypted = [self.decryptor updateData:nil isFinal:YES];
//    NSString * str = [[NSString alloc] initWithBytes:decrypted.bytes
//                                              length:decrypted.length
//                                            encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",str);
    [self updateStatus:@"Completed decryption"];
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    NSURL * dropBoxURL = [NSURL URLWithString:@"https://dropbox.com"];
    NSURLRequest * req = [NSURLRequest requestWithURL:dropBoxURL];
    [webView loadRequest:req];
    
//    NSURL * fileURL = [NSURL URLWithString:@"kkcrypto://files.parsetfss.com/5cd82b2a-5088-45cc-a262-73769d6a0d26/tfss-81c87d23-9e28-45b9-b1e9-328cc485f5dd-eagle_nebula.jpg"];
//    
//    NSData * salt = [[NSData alloc] initWithBase64EncodedString:@"tXOmYW2Tv2c=" options:-1];
//    NSString * iv = @"oNhCJrd2clGdGX9Xv67hRw==";
//    
//    NSData * encryptionKey = [KKCrypto deriveKeyUsingSpecification:[KKCryptoKeyDerivationSpec AES256]
//                                                      fromPassword:USER_PASSWORD
//                                                         usingSalt:salt];
//    NSLog(@"encryptionKey: %@",encryptionKey);
//
//    NSString * strEncryptionKey = [encryptionKey base64EncodedStringWithOptions:-1];
//    
//    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:fileURL];
//    
//    [request setValue:strEncryptionKey forHTTPHeaderField:kKKCryptoKeyEncryptionKey];
//    [request setValue:iv forHTTPHeaderField:kKKCryptoKeyIV];
//    [request setValue:[NSString stringWithFormat:@"%d",kKKCryptoTaskDecryption] forHTTPHeaderField:kKKCryptoKeyTaskType];
//    
////    self.decryptor  = [KKCryptor cryptorForCryptoTask:kKKCryptoTaskDecryption
////                                        Specification:[KKCryptoCryptorSpec new]
////                                        EncryptionKey:encryptionKey HMACKey:nil
////                                                   IV:[[NSData alloc] initWithBase64EncodedString:iv options:-1]];
//    [webView setScalesPageToFit:YES];
//    [webView loadRequest:request];
    
    
    //self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    
    
// Give the file that needs to be uploaded
// After decryption you will get the objectID printed in the console.
    
//    
//    NSString * orange = [[NSBundle mainBundle] pathForResource:@"orange" ofType:@"txt"];
//    [self uploadFileFromPath:orange byEncryptingOnTheFly:YES];
//
//    
    // 7rHiUKnb5g  16  Bytes
    // y6r7CsxUUd   8  Bytes
    // J0T8lGC5da Big  Files
    // RiOZG8ZSX2   0  Bytes
    // Dm3UyOoJGF 4MB  Eagle Nebula.jpg
    
    
// FileID to be downloaded from the server for decryption
//    [self decryptFileWithID:@"J0T8lGC5da"];
}


- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - Decryption Tasks


- (void) decryptFileWithID:(NSString *)fileID {
    [self updateStatus:@"Retrieving file with ID"];
    [self updateStatus:fileID];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://api.parse.com/1/classes/encryptedfile/%@",fileID]]];
    [request setHTTPMethod:@"GET"];
    [request setValue:PARSE_APPLICATION_ID forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:PARSE_REST_API_KEY forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSDictionary * dict  = [NSJSONSerialization JSONObjectWithData:data options:-1 error:nil];
        [self updateStatus:@"File details retrieved..."];
        PAPIEncryptedFile * encryptedFile = [PAPIEncryptedFile encryptedFileFromDictionary:dict];
        [self decryptFileOnTheFly:encryptedFile];
    }];
    
}




- (void) decryptFileOnTheFly:(PAPIEncryptedFile *)encryptedFile {
    [self updateStatus:@"Decryption of file started..."];
    NSURL * fileUrl = [NSURL URLWithString:encryptedFile.ekFileURL];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:fileUrl];
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
    
    KKCryptoCryptorSpec * cryptoSpec = [KKCryptoCryptorSpec specAES256];
    
    NSData * encryptionKey = [KKCrypto deriveKeyUsingSpecification:cryptoSpec.keySpecEncryption
                                                      fromPassword:USER_PASSWORD
                                                         usingSalt:encryptedFile.ekSalt];
    
    self.decryptor  = [KKCryptor cryptorForCryptoTask:kKKCryptoTaskDecryption
                                        Specification:cryptoSpec
                                        EncryptionKey:encryptionKey HMACKey:nil
                                                   IV:encryptedFile.ekIV];
}





#pragma mark -
#pragma mark - Encryption Tasks

- (void) uploadFileFromPath:(NSString *)filePath byEncryptingOnTheFly:(BOOL)shouldEncrypt {
    
    [self updateStatus:@"Generating salt and iv..."];
    
    KKCryptoCryptorSpec * cryptoSpec = [KKCryptoCryptorSpec specAES256];
    NSData * encryptionKeySalt       = [KKCrypto randomDataOfLength:cryptoSpec.keySpecEncryption.saltSize];
    NSData * iv                      = [KKCrypto randomDataOfLength:cryptoSpec.ivSize];
    
    [self updateStatus:@"Encryption salt and iv created..."];
    
    NSData * encryptionKey = [KKCrypto deriveKeyUsingSpecification:cryptoSpec.keySpecEncryption
                                                      fromPassword:USER_PASSWORD
                                                         usingSalt:encryptionKeySalt];
    
    KKCryptor * encryptor = [KKCryptor cryptorForCryptoTask:kKKCryptoTaskEncryption
                                              Specification:cryptoSpec
                                              EncryptionKey:encryptionKey
                                                    HMACKey:nil IV:iv];
    
    NSString * fileName = [filePath lastPathComponent];
    
    [self updateStatus:@"File selected for encryption..."];
    [self updateStatus:fileName];
    
    NSInputStream * plainStream         = [NSInputStream inputStreamWithFileAtPath:filePath];
    KKCryptoInputStream * cryptoStream  = [[KKCryptoInputStream alloc] initWithPlainNSInputStream:plainStream];
    [cryptoStream setCryptor:encryptor];
    
    NSURL * uploadURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.parse.com/1/files/%@",fileName]];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:uploadURL];
    [request setValue:PARSE_APPLICATION_ID forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:PARSE_REST_API_KEY forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBodyStream:cryptoStream];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (!connectionError) {
            NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:-1 error:nil];
            PAPIEncryptedFile * encryptedFile = [[PAPIEncryptedFile alloc] init];
            encryptedFile.ekFileName = [dict objectForKey:@"name"];
            encryptedFile.ekFileURL  = [dict objectForKey:@"url"];
            encryptedFile.ekSalt     = encryptionKeySalt;
            encryptedFile.ekIV       = iv;
            [self updateStatus:@"File uploaded..."];
            [self updateStatus:dict.description];
            [self updateStatus:@""];
            [self makeEntryForEncryptedFile:encryptedFile];
        } else {
            [self updateStatus:[NSString stringWithFormat:@"Error updating file : %@",connectionError.debugDescription]];
        }
        
    }];
}



- (void) makeEntryForEncryptedFile:(PAPIEncryptedFile *)encryptedFile {
    
    NSDictionary * encFileJson    = [encryptedFile jsonObject];
    NSData       * httpBody       = [NSJSONSerialization dataWithJSONObject:encFileJson options:-1 error:nil];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.parse.com/1/classes/encryptedfile"]];
    [self updateStatus:@"Make entry for the file using below data"];
    [self updateStatus:encFileJson.description];
    
    [request setHTTPBody:httpBody];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%ld",httpBody.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:PARSE_APPLICATION_ID forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:PARSE_REST_API_KEY forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:-1 error:nil];
        [self updateStatus:@"Entry made successfully"];
        [self updateStatus:dict.description];
        [self updateStatus:@"Encryption and uploading completed successfully!"];
        [self updateStatus:@"You can use the URL to download the encrypted file!"];
        [self updateStatus:@"\n"];
        NSLog(@"%@",dict.description);
    }];
    
}



@end
