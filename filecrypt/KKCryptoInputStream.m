//
//  KKCryptoInputStream.m
//  filecrypt
//
//  Created by Karthik Kumar S on 4/22/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import "KKCryptoInputStream.h"
#import "KKCryptor.h"

@interface KKCryptoInputStream ()

@property (nonatomic,strong) KKCryptor * cryptor;

@end

@implementation KKCryptoInputStream 
{
	NSInputStream            * plainStream;
	id <NSStreamDelegate>      delegate;
	
	CFReadStreamClientCallBack copiedCallback;
	CFStreamClientContext      copiedContext;
	CFOptionFlags              requestedEvents;
}


#pragma mark -
#pragma mark - Stream LifeCycle

- (id)initWithPlainNSInputStream:(NSInputStream *)stream
{
    self = [super init];
    if (self) {
        plainStream = stream;
        plainStream.delegate = self;
        [self setDelegate:self];
    }
    return self;
}

- (void)dealloc
{
    self.cryptor = nil;
    plainStream = nil;
}

#pragma mark NSStream subclass methods

- (void)open {
    [self.cryptor initiateCryptor];
	[plainStream open];
}

- (void)close {
    [self.cryptor finalizeCryptor];
	[plainStream close];
}

- (id <NSStreamDelegate> )delegate {
	return delegate;
}

- (void)setDelegate:(id<NSStreamDelegate>)aDelegate {
	if (aDelegate == nil) {
		delegate = self;
	}
	else {
		delegate = aDelegate;
	}
}

- (void)scheduleInRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode {
	[plainStream scheduleInRunLoop:aRunLoop forMode:mode];
}

- (void)removeFromRunLoop:(NSRunLoop *)aRunLoop forMode:(NSString *)mode {
	[plainStream removeFromRunLoop:aRunLoop forMode:mode];
}

- (id)propertyForKey:(NSString *)key {
	return [plainStream propertyForKey:key];
}

- (BOOL)setProperty:(id)property forKey:(NSString *)key {
	return [plainStream setProperty:property forKey:key];
}

- (NSStreamStatus)streamStatus {
	return [plainStream streamStatus];
}

- (NSError *)streamError {
	return [plainStream streamError];
}

#pragma mark NSInputStream subclass methods

- (NSInteger)read:(uint8_t *)buffer maxLength:(NSUInteger)len {
    
    NSMutableData * nonCryptedData = [NSMutableData dataWithLength:len];
    NSInteger nonCryptedBytesRead  = [plainStream read:nonCryptedData.mutableBytes maxLength:len];
    NSData *  encryptedData        = nil;
    BOOL finalCalled;
    
    if ([self hasBytesAvailable]) {
        encryptedData = [self.cryptor updateData:[nonCryptedData subdataWithRange:NSMakeRange(0, nonCryptedBytesRead)] isFinal:false];
    } else {
        encryptedData = [self.cryptor updateData:[nonCryptedData subdataWithRange:NSMakeRange(0, nonCryptedBytesRead)] isFinal:true];
        finalCalled   = YES;
    }
    
    if (!finalCalled && encryptedData.length==0) {
        encryptedData = [self.cryptor updateData:[nonCryptedData subdataWithRange:NSMakeRange(0, nonCryptedBytesRead)] isFinal:true];
    }
    
    if (encryptedData) {
        memcpy(buffer, encryptedData.bytes, encryptedData.length);
    }
    
    return encryptedData.length;
}


- (BOOL)getBuffer:(uint8_t **)buffer length:(NSUInteger *)len {
	return NO;
}


- (BOOL)hasBytesAvailable {
	return [plainStream hasBytesAvailable];
}


#pragma mark Undocumented CFReadStream bridged methods

- (void)_scheduleInCFRunLoop:(CFRunLoopRef)aRunLoop forMode:(CFStringRef)aMode {

	CFReadStreamScheduleWithRunLoop((CFReadStreamRef)plainStream, aRunLoop, aMode);
}


- (BOOL)_setCFClientFlags:(CFOptionFlags)inFlags
                 callback:(CFReadStreamClientCallBack)inCallback
                  context:(CFStreamClientContext *)inContext {
	
	if (inCallback != NULL) {
		requestedEvents = inFlags;
		copiedCallback = inCallback;
		memcpy(&copiedContext, inContext, sizeof(CFStreamClientContext));
		
		if (copiedContext.info && copiedContext.retain) {
			copiedContext.retain(copiedContext.info);
		}
	}
	else {
		requestedEvents = kCFStreamEventNone;
		copiedCallback = NULL;
		if (copiedContext.info && copiedContext.release) {
			copiedContext.release(copiedContext.info);
		}
		
		memset(&copiedContext, 0, sizeof(CFStreamClientContext));
	}
	
	return YES;	
}


- (void)_unscheduleFromCFRunLoop:(CFRunLoopRef)aRunLoop forMode:(CFStringRef)aMode {

	CFReadStreamUnscheduleFromRunLoop((CFReadStreamRef)plainStream, aRunLoop, aMode);
}


#pragma mark NSStreamDelegate methods

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode {
    
    NSLog(@"Handle Event: %ld",eventCode);
	
	assert(aStream == plainStream);
	
	switch (eventCode) {
		case NSStreamEventOpenCompleted:
			if (requestedEvents & kCFStreamEventOpenCompleted) {
				copiedCallback((__bridge CFReadStreamRef)self,
							   kCFStreamEventOpenCompleted,
							   copiedContext.info);
			}
			break;
			
		case NSStreamEventHasBytesAvailable:
			if (requestedEvents & kCFStreamEventHasBytesAvailable) {
				copiedCallback((__bridge CFReadStreamRef)self,
							   kCFStreamEventHasBytesAvailable,
							   copiedContext.info);
			}
			break;
			
		case NSStreamEventErrorOccurred:
			if (requestedEvents & kCFStreamEventErrorOccurred) {
				copiedCallback((__bridge CFReadStreamRef)self,
							   kCFStreamEventErrorOccurred,
							   copiedContext.info);
			}
			break;
			
		case NSStreamEventEndEncountered:
			if (requestedEvents & kCFStreamEventEndEncountered) {
				copiedCallback((__bridge CFReadStreamRef)self,
							   kCFStreamEventEndEncountered,
							   copiedContext.info);
			}
			break;
			
		case NSStreamEventHasSpaceAvailable:
			break;
			
		default:
			break;
	}
}


@end
