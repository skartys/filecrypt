//
//  PAPIEncryptedFile.h
//  filecrypt
//
//  Created by Karthik Kumar S on 4/22/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAPIEncryptedFile : NSObject

+ (instancetype) encryptedFileFromDictionary:(NSDictionary *)dictionary;
- (NSDictionary *) jsonObject;

@property (nonatomic,strong) NSString * ekFileName;
@property (nonatomic,strong) NSString * ekFileURL;
@property (nonatomic,strong) NSData   * ekSalt;
@property (nonatomic,strong) NSData   * ekIV;

@end
