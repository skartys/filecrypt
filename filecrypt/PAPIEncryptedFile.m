//
//  PAPIEncryptedFile.m
//  filecrypt
//
//  Created by Karthik Kumar S on 4/22/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import "PAPIEncryptedFile.h"

@implementation PAPIEncryptedFile


+ (instancetype) encryptedFileFromDictionary:(NSDictionary *)dictionary {
    @autoreleasepool {
        PAPIEncryptedFile * encryptedFile = [[PAPIEncryptedFile alloc] init];
        encryptedFile.ekFileURL  = [dictionary objectForKey:@"ekFileURL"];
        encryptedFile.ekFileName = [dictionary objectForKey:@"ekFileName"];
        NSString * base64Salt    = [dictionary objectForKey:@"ekSalt"];
        NSString * base64IV      = [dictionary objectForKey:@"ekIV"];
        encryptedFile.ekSalt     = [[NSData alloc] initWithBase64EncodedString:base64Salt options:-1];
        encryptedFile.ekIV       = [[NSData alloc] initWithBase64EncodedString:base64IV options:-1];
        return encryptedFile;
    }
}



- (NSDictionary *) jsonObject {
    @autoreleasepool {
        NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
        [dictionary setObject:self.ekFileName forKey:@"ekFileName"];
        [dictionary setObject:self.ekFileURL forKey:@"ekFileURL"];
        [dictionary setObject:[self.ekSalt base64EncodedStringWithOptions:-1] forKey:@"ekSalt"];
        [dictionary setObject:[self.ekIV base64EncodedStringWithOptions:-1] forKey:@"ekIV"];
        return dictionary;
    }
}

@end
