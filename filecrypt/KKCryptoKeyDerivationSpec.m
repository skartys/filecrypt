//
//  KKCryptoKeyDerivationSpec.m
//  filecrypt
//
//  Created by Karthik Kumar S on 4/21/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import "KKCryptoKeyDerivationSpec.h"

@implementation KKCryptoKeyDerivationSpec

+ (instancetype) AES256 {
    @autoreleasepool {
        KKCryptoKeyDerivationSpec * kSpec = [[KKCryptoKeyDerivationSpec alloc] init];
        kSpec.keySize  = kKKKeySizeAES256;
        kSpec.saltSize = 8;
        kSpec.keyDerivationAlgorithm = kKKeyDerivationPBKDF2;
        kSpec.prfHashAlgorithm = kKKPseudoRandomAlgorithmSHA256;
        kSpec.noOfRounds = 12000;
        return kSpec;
    }
}

@end
