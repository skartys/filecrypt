//
//  KKCryptor.h
//  filecrypt
//
//  Created by Karthik Kumar S on 4/22/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KKCryptoConstants.h"

@class KKCryptoCryptorSpec;

@interface KKCryptor : NSObject

+ (instancetype) cryptorForCryptoTask:(KKCryptoTask)cryptoTask
                        Specification:(KKCryptoCryptorSpec *)specification
                        EncryptionKey:(NSData *)encryptionKey
                              HMACKey:(NSData *)hmacKey
                                   IV:(NSData *)iv;

- (void) initiateCryptor;

- (NSData *) updateData:(NSData *)data isFinal:(BOOL)isFinal;

- (void) finalizeCryptor;

@end
