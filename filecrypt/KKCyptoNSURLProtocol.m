//
//  KKCyptoNSURLProtocol.m
//  filecrypt
//
//  Created by Karthik Kumar S on 4/23/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import "KKCyptoNSURLProtocol.h"
#import "KKCryptoCryptorSpec.h"
#import "KKCryptor.h"

@interface KKCyptoNSURLProtocol() <NSURLConnectionDataDelegate, NSURLConnectionDelegate>

@property (nonatomic,strong) NSString           * encryptionKey;
@property (nonatomic,strong) NSString           * encryptionIV;
@property (nonatomic,strong) NSString           * cryptoTaskType;
@property (nonatomic,strong) KKCryptor          * cryptor;
@property (nonatomic,strong) NSURLConnection    * connection;

@end

@implementation KKCyptoNSURLProtocol


+ (BOOL) canInitWithRequest:(NSURLRequest *)request {
    if (![[request.URL scheme] isEqualToString:@"kkcrypto"]) return NO;
    NSString * base64EncryptionKey  = [request valueForHTTPHeaderField:kKKCryptoKeyEncryptionKey];
    NSString * base64EncyptionIV    = [request valueForHTTPHeaderField:kKKCryptoKeyIV];
    NSString * cryptoTaskID         = [request valueForHTTPHeaderField:kKKCryptoKeyTaskType];
    //NSLog(@"%@ \n %@ \n %@ \n",base64EncryptionKey,base64EncyptionIV,cryptoTaskID);
    return (base64EncryptionKey!=nil && base64EncyptionIV!=nil && cryptoTaskID!=nil);
}



+ (NSURLRequest *) canonicalRequestForRequest:(NSURLRequest *)request {
    return request;
}


- (void) startLoading {
    self.encryptionKey  = [self.request valueForHTTPHeaderField:kKKCryptoKeyEncryptionKey];
    self.encryptionIV    = [self.request valueForHTTPHeaderField:kKKCryptoKeyIV];
    self.cryptoTaskType   = [self.request valueForHTTPHeaderField:kKKCryptoKeyTaskType];
    
    self.cryptor = [KKCryptor cryptorForCryptoTask:[self.cryptoTaskType intValue]
                                     Specification:[KKCryptoCryptorSpec specAES256]
                                     EncryptionKey:[[NSData alloc] initWithBase64EncodedString:self.encryptionKey options:-1]
                                           HMACKey:nil
                                                IV:[[NSData alloc] initWithBase64EncodedString:self.encryptionIV options:-1]];
    [self.cryptor initiateCryptor];
    NSMutableURLRequest * cryptoRequest = [self.request mutableCopy];
    NSString * urlWithCryptoScheme      = [cryptoRequest.URL absoluteString];
    urlWithCryptoScheme = [urlWithCryptoScheme stringByReplacingOccurrencesOfString:@"kkcrypto" withString:@"https"];
    [cryptoRequest setURL:[NSURL URLWithString:urlWithCryptoScheme]];
    self.connection = [NSURLConnection connectionWithRequest:cryptoRequest delegate:self];
}


- (void) stopLoading {
    [self.connection cancel];
    self.cryptor = nil;
}



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [self.client URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
}



- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSData * deCryptedData = [self.cryptor updateData:data isFinal:NO];
    [self.client URLProtocol:self didLoadData:deCryptedData];
}



- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSData * deCryptedData = [self.cryptor updateData:nil isFinal:YES];
    if (deCryptedData) {
        [self.client URLProtocol:self didLoadData:deCryptedData];
    }
    [self.client URLProtocolDidFinishLoading:self];
}



- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [self.client URLProtocol:self didFailWithError:error];
}

@end
