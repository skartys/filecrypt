//
//  KKCryptoConstants.h
//  filecrypt
//
//  Created by Karthik Kumar S on 4/20/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef uint32_t KKCryptoAlgorithm;
typedef uint32_t KKCryptoKeySize;
typedef uint32_t KKCryptoBlockSize;
typedef uint32_t KKCryptoKeyDerivationAlgorithm;
typedef uint32_t KKCryptoPseudoRandomAlgorithm;
typedef uint32_t KKCryptoTask;
typedef uint32_t KKCryptoPadding;
typedef uint32_t KKCryptoHMACAlgorithm;
typedef uint     KKCryptoRounds;
typedef size_t   KKCryptoSaltSize;
typedef size_t   KKCryptoIVSize;
typedef size_t   KKCryptoHMACDigestLength;


NSString * const    kKKCryptoKeyEncryptionKey;
NSString * const    kKKCryptoKeyIV;
NSString * const    kKKCryptoKeyTaskType;

const KKCryptoAlgorithm                 kKKAlgorithmAES;

const KKCryptoKeySize                   kKKKeySizeAES256;

const KKCryptoBlockSize                 kKKBlockSizeAES;

const KKCryptoKeyDerivationAlgorithm    kKKeyDerivationPBKDF2;

const KKCryptoPseudoRandomAlgorithm     kKKPseudoRandomAlgorithmSHA256;

const KKCryptoPadding                   kKKPaddingPKCS7;

const KKCryptoHMACAlgorithm             kKKHMACSHA256;

const KKCryptoHMACDigestLength          kKKHMACSHA256DigestLength;

const KKCryptoTask kKKCryptoTaskEncryption;
const KKCryptoTask kKKCryptoTaskDecryption;

@interface KKCryptoConstants : NSObject

@end
