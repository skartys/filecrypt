//
//  KKCryptoCryporSpec.h
//  filecrypt
//
//  Created by Karthik Kumar S on 4/21/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KKCryptoConstants.h"

@class KKCryptoKeyDerivationSpec;

@interface KKCryptoCryptorSpec : NSObject

+ (instancetype) specAES256;

@property (nonatomic) KKCryptoAlgorithm         algorithm;
@property (nonatomic) KKCryptoBlockSize         blockSize;
@property (nonatomic) KKCryptoIVSize            ivSize;
@property (nonatomic) KKCryptoPadding           padding;
@property (nonatomic) KKCryptoHMACAlgorithm     hmacAlgorithm;
@property (nonatomic) KKCryptoHMACDigestLength  hmacDigestLength;

@property (nonatomic,strong) KKCryptoKeyDerivationSpec * keySpecEncryption;
@property (nonatomic,strong) KKCryptoKeyDerivationSpec * keySpecHMAC;


@end
