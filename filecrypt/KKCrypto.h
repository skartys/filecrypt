//
//  KKCrypto.h
//  filecrypt
//
//  Created by Karthik Kumar S on 4/21/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#define KKCryptoAESEncrypt(data,key) [KKCrypto performTask:kKKCryptoTaskEncryption onData:data usingKey:key];
#define KKCryptoAESDecrypt(data,key) [KKCrypto performTask:kKKCryptoTaskDecryption onData:data usingKey:key];
#define KKCryptoHMACSHA256(data,key) [KKCrypto hmacSHA256OfData:data usingKey:key];

#import <Foundation/Foundation.h>
#import "KKCryptoConstants.h"

@class KKCryptoKeyDerivationSpec;

@interface KKCrypto : NSObject

+ (NSData *) deriveKeyUsingSpecification:(KKCryptoKeyDerivationSpec *)spec
                            fromPassword:(NSString *)password
                               usingSalt:(NSData *)salt;


+ (NSData *) randomDataOfLength:(size_t)length;


+ (NSData *) performTask:(KKCryptoTask)task onData:(NSData *)data usingKey:(NSData *)key;


+ (NSData *) sha256HashOfData:(NSData *)data;


+ (NSData *) hmacSHA256OfData:(NSData *)data usingKey:(NSData *)key;


@end
