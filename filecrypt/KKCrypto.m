//
//  KKCrypto.m
//  filecrypt
//
//  Created by Karthik Kumar S on 4/21/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//


#import "KKCrypto.h"
#import <CommonCrypto/CommonCrypto.h>
#import "KKCryptoKeyDerivationSpec.h"

@implementation KKCrypto


+ (NSData *) performTask:(KKCryptoTask)task onData:(NSData *)data usingKey:(NSData *)key {
    
    size_t bufferLength = data.length + kCCBlockSizeAES128;
    size_t dataOutLength;
    NSMutableData * buffer = [NSMutableData dataWithLength:bufferLength];
    int status = CCCrypt(task,
                         kKKAlgorithmAES,                   // Encryption or Decryption
                         kKKPaddingPKCS7,                   // Padding to use
                         key.bytes, key.length,             // Key
                         NULL,                              // Use NULL iv (0000 0000 0000 0000)
                         data.bytes, data.length,           // data in bytes
                         buffer.mutableBytes, bufferLength, // buffer
                         &dataOutLength);                   // Original output length
    if (status!=kCCSuccess) {
        return nil;
    }
    return [buffer subdataWithRange:NSMakeRange(0, dataOutLength)];
}



+ (NSData *) sha256HashOfData:(NSData *)data {
    uint8_t sha256Hash[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(data.bytes, CC_SHA256_DIGEST_LENGTH, sha256Hash);
    return [NSData dataWithBytes:sha256Hash length:CC_SHA256_DIGEST_LENGTH];
}



+ (NSData *) hmacSHA256OfData:(NSData *)data usingKey:(NSData *)key {
    uint8_t hmacSHA256[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, key.bytes, key.length, data.bytes, data.length, hmacSHA256);
    return [NSData dataWithBytes:hmacSHA256 length:CC_SHA256_DIGEST_LENGTH];
}



+ (NSData *) deriveKeyUsingSpecification:(KKCryptoKeyDerivationSpec *)spec
                            fromPassword:(NSString *)password
                               usingSalt:(NSData *)salt
{
    // allocate space for storing the derived key
    uint8_t key[spec.keySize];
    
    // convert the password to data
    NSData * pd = [password dataUsingEncoding:NSUTF8StringEncoding];
    
    // variable to hold the status returned by the key derivation function
    int status;
    
    if (spec.keyDerivationAlgorithm==kKKeyDerivationPBKDF2) {
        status = CCKeyDerivationPBKDF(spec.keyDerivationAlgorithm,              // algorithm to use
                                      pd.bytes, pd.length,                      // password inputs
                                      salt.bytes, salt.length,                  // salt inputs
                                      spec.prfHashAlgorithm, spec.noOfRounds,   // hash algorithm
                                      key, spec.keySize);                       // key output
    }
    
    NSAssert(status==kCCSuccess, @"key derivation failed");
    
    return [NSData dataWithBytes:key length:spec.keySize];
}



+ (NSData *) randomDataOfLength:(size_t)length {
    
    uint8_t   randomBytes[length];
    int       status;
    status = SecRandomCopyBytes(kSecRandomDefault, length, randomBytes);
   
    NSAssert(status==0, @"deriving random data failed");
    
    NSData * randomData = [NSData dataWithBytes:randomBytes length:length];
    return randomData;
}


@end
