//
//  ViewController.h
//  filecrypt
//
//  Created by Karthik Kumar S on 4/15/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    IBOutlet UITextView * statusView;
    IBOutlet UIButton   * taskButton;
    IBOutlet UIWebView  * webView;
}


@end

