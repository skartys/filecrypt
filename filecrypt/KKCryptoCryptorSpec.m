//
//  KKCryptoCryporSpec.m
//  filecrypt
//
//  Created by Karthik Kumar S on 4/21/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import "KKCryptoCryptorSpec.h"
#import "KKCryptoKeyDerivationSpec.h"

@implementation KKCryptoCryptorSpec

+ (instancetype) specAES256 {
    
    @autoreleasepool {
        
        KKCryptoCryptorSpec * cryptorSpec = [[KKCryptoCryptorSpec alloc] init];
        
        cryptorSpec.algorithm       = kKKAlgorithmAES;
        cryptorSpec.blockSize       = kKKBlockSizeAES;
        cryptorSpec.ivSize          = kKKBlockSizeAES;
        cryptorSpec.padding         = kKKPaddingPKCS7;
        cryptorSpec.hmacAlgorithm   = kKKHMACSHA256;
        cryptorSpec.hmacDigestLength= kKKHMACSHA256DigestLength;
        
        KKCryptoKeyDerivationSpec * keyDerivationSpec   = [[KKCryptoKeyDerivationSpec alloc] init];
        keyDerivationSpec.keySize                       = kKKKeySizeAES256;
        keyDerivationSpec.saltSize                      = 8;
        keyDerivationSpec.keyDerivationAlgorithm        = kKKeyDerivationPBKDF2;
        keyDerivationSpec.prfHashAlgorithm              = kKKPseudoRandomAlgorithmSHA256;
        keyDerivationSpec.noOfRounds                    = 12000;
        
        KKCryptoKeyDerivationSpec * hmacKeyDerivationSpec   = [[KKCryptoKeyDerivationSpec alloc] init];
        hmacKeyDerivationSpec.keySize                       = kKKKeySizeAES256;
        hmacKeyDerivationSpec.saltSize                      = 8;
        hmacKeyDerivationSpec.keyDerivationAlgorithm        = kKKeyDerivationPBKDF2;
        hmacKeyDerivationSpec.prfHashAlgorithm              = kKKPseudoRandomAlgorithmSHA256;
        hmacKeyDerivationSpec.noOfRounds                    = 12000;

        cryptorSpec.keySpecEncryption = keyDerivationSpec;
        cryptorSpec.keySpecHMAC       = hmacKeyDerivationSpec;
        
        return cryptorSpec;
    }
    
}

@end
