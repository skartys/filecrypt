//
//  KKCryptoKeyDerivationSpec.h
//  filecrypt
//
//  Created by Karthik Kumar S on 4/21/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KKCryptoConstants.h"

@interface KKCryptoKeyDerivationSpec : NSObject

+ (instancetype) AES256;

@property (nonatomic) KKCryptoKeySize                   keySize;
@property (nonatomic) KKCryptoSaltSize                  saltSize;
@property (nonatomic) KKCryptoKeyDerivationAlgorithm    keyDerivationAlgorithm;
@property (nonatomic) KKCryptoPseudoRandomAlgorithm     prfHashAlgorithm;
@property (nonatomic) KKCryptoRounds                    noOfRounds;

@end
