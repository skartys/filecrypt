//
//  KKCryptoConstants.m
//  filecrypt
//
//  Created by Karthik Kumar S on 4/20/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import "KKCryptoConstants.h"
#import <CommonCrypto/CommonCrypto.h>

NSString * const    kKKCryptoKeyEncryptionKey = @"KKCryptoKeyEncryptionKey";
NSString * const    kKKCryptoKeyIV            = @"KKCryptoIV";
NSString * const    kKKCryptoKeyTaskType      = @"KKCryptoTaskType";

const KKCryptoAlgorithm                 kKKAlgorithmAES = kCCAlgorithmAES;
const KKCryptoKeySize                   kKKBlockSizeAES = kCCBlockSizeAES128;
const KKCryptoBlockSize                 kKKKeySizeAES256 = kCCKeySizeAES256;
const KKCryptoKeyDerivationAlgorithm    kKKeyDerivationPBKDF2 = kCCPBKDF2;
const KKCryptoPseudoRandomAlgorithm     kKKPseudoRandomAlgorithmSHA256 = kCCPRFHmacAlgSHA256;
const KKCryptoPadding                   kKKPaddingPKCS7 = kCCOptionPKCS7Padding;
const KKCryptoHMACAlgorithm             kKKHMACSHA256 = kCCHmacAlgSHA256;
const KKCryptoHMACDigestLength          kKKHMACSHA256DigestLength = CC_SHA256_DIGEST_LENGTH;

const KKCryptoTask kKKCryptoTaskEncryption = kCCEncrypt;
const KKCryptoTask kKKCryptoTaskDecryption = kCCDecrypt;

@implementation KKCryptoConstants

@end
