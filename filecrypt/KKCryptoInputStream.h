
//
//  KKCryptoInputStream.h
//  filecrypt
//
//  Created by Karthik Kumar S on 4/22/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//


#import <Foundation/Foundation.h>

@class KKCryptor;

@interface KKCryptoInputStream : NSInputStream <NSStreamDelegate>

- (instancetype)initWithPlainNSInputStream:(NSInputStream *)stream;

- (void) setCryptor:(KKCryptor *)crypto;

@end
