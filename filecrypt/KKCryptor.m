//
//  KKCryptor.m
//  filecrypt
//
//  Created by Karthik Kumar S on 4/22/15.
//  Copyright (c) 2015 Karthik Kumar S. All rights reserved.
//

#import "KKCryptor.h"
#import <CommonCrypto/CommonCrypto.h>
#import "KKCryptoCryptorSpec.h"
#import "KKCryptoKeyDerivationSpec.h"

#define FINAL_BUFFER_SIZE 256

@interface KKCryptor ()

@property (nonatomic,strong) KKCryptoCryptorSpec * spec;
@property (nonatomic,assign) KKCryptoTask cryptoTask;
@property (nonatomic,strong) NSData * encryptionKey;
@property (nonatomic,strong) NSData * IV;
@property (nonatomic,strong) NSData * HMACKey;

@end

@implementation KKCryptor {
    CCCryptorRef _cryptorContext;
    BOOL         _hasWrittenHeader;
}


- (CCCryptorRef *) cryptoContext {
    return &_cryptorContext;
}


+ (instancetype) cryptorForCryptoTask:(KKCryptoTask)cryptoTask
                        Specification:(KKCryptoCryptorSpec *)specification
                        EncryptionKey:(NSData *)encryptionKey
                              HMACKey:(NSData *)hmacKey
                                   IV:(NSData *)iv
{
    @autoreleasepool {
        KKCryptor * cryptor = [[KKCryptor alloc] init];
        cryptor.cryptoTask      = cryptoTask;
        cryptor.spec            = specification;
        cryptor.encryptionKey   = encryptionKey;
        cryptor.IV              = iv;
        cryptor.HMACKey         = hmacKey;
        return cryptor;
    }
}


- (void) initiateCryptor {
    
    int status;
    
    if (!_spec) {
        self.spec = [KKCryptoCryptorSpec specAES256];
    }
    
        status = CCCryptorCreate(self.cryptoTask,
                                 self.spec.algorithm,
                                 self.spec.padding,
                                 self.encryptionKey.bytes, self.encryptionKey.length,
                                 self.IV.bytes, &_cryptorContext);
    
    NSAssert(status==kCCSuccess, @"Initiating the cryptor failed");
}


- (NSData *) updateData:(NSData *)data isFinal:(BOOL)isFinal {
    
    size_t dataInLength;
    size_t dataOutAvailable;
    size_t dataOutMoved;
    NSData * outData = nil;
    
    int status;
    
    if (!isFinal) {
        dataInLength = data.length;
        dataOutAvailable = CCCryptorGetOutputLength(_cryptorContext, dataInLength, true);
        uint8_t dataOut[dataOutAvailable];
        status = CCCryptorUpdate(_cryptorContext, data.bytes, dataInLength, dataOut, dataOutAvailable, &dataOutMoved);
        NSAssert(status==kCCSuccess, @"updating data to the cryptor has failed");
        outData = [NSData dataWithBytes:dataOut length:dataOutMoved];
    } else {
        uint8_t dataOut[FINAL_BUFFER_SIZE];
        status = CCCryptorFinal(_cryptorContext, dataOut, FINAL_BUFFER_SIZE, &dataOutMoved);
        NSAssert(status==kCCSuccess, @"finalizing the cryptor has failed");
        outData = [NSData dataWithBytes:dataOut length:dataOutMoved];
    }

    return outData;
}



- (void) finalizeCryptor {
    //CCCryptorRelease(_cryptorContext);
}


- (void) dealloc {
    CCCryptorRelease(_cryptorContext);
}

@end
